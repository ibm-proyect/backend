﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api_dotnet.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("users")]
    public class TestController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult List()
        {
            try
            {
                string url = "https://jsonplaceholder.typicode.com/users";
                string result = GetData(url);
                
                if(result.ToLower() != "error")
                {
                    List<object> json = JsonConvert.DeserializeObject<List<object>>(result).ToList();
                    return Ok(json);
                }
                else
                {
                    return BadRequest("Ha ocurrido un error");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest("Ha ocurrido un error");
            }
        }
        public static string GetData(string url)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.ContentType = "application/json";
                webRequest.Method = "GET";

                HttpWebResponse httpResponse = (HttpWebResponse)webRequest.GetResponse();
                using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string result = streamReader.ReadToEnd();
                    return (result);
                }
            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message);
                return "error";
            }
        }
    }
}

